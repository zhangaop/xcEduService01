package com.xuecheng.framework.domain.cms;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;

/**
 * @Author: mrt.
 * @Description:
 * @Date:Created in 2018/1/24 10:04.
 * @Modified By:
 */
@Data
@ToString
@Document(collection = "cms_page")
public class CmsPage {
    /**
     * 页面名称、别名、访问地址、类型（静态/动态）、页面模版、状态
     */
    //站点ID
    // @Field("")
    private String siteId;
    //页面ID
    @Id
    // @Field("")
    private String pageId;
    //页面名称
    // @Field("")
    private String pageName;
    //别名
    // @Field("")
    private String pageAliase;
    //访问地址
    // @Field("")
    private String pageWebPath;
    //参数
    // @Field("")
    private String pageParameter;
    //物理路径
    // @Field("")
    private String pagePhysicalPath;
    //类型（静态/动态）
    // @Field("")
    private String pageType;
    //页面模版
    // @Field("")
    private String pageTemplate;
    //页面静态化内容
    // @Field("")
    private String pageHtml;
    //状态
    // @Field("")
    private String pageStatus;
    //创建时间
    // @Field("")
    private Date pageCreateTime;
    //模版id
    // @Field("")
    private String templateId;
    //参数列表
    // @Field("")
    private List<CmsPageParam> pageParams;
    //模版文件Id
//    private String templateFileId;
    //静态文件Id
    // @Field("")
    private String htmlFileId;
    //数据Url
    // @Field("")
    private String dataUrl;



}
