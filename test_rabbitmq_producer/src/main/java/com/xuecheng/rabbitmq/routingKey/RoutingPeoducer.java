package com.xuecheng.rabbitmq.routingKey;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-09-14-20-19
 *
 * 每个消费者监听自己的队列,并且设置routingkey
 * 生产者将消息发给交换机,有交换机根据routingkey来转发消息到指定的队列
 *
 */
public class RoutingPeoducer {
    private static final String QUEUE = "HELLO_WORLD";
    private static final String key_queue_inform_email = "key_queue_inform_email";
    private static final String key_queue_inform_sms = "key_queue_inform_sms";
    //不处理路由键交换机名称
    private static final String EXCHANGE_ROUTING_INFORM = "exchange_routing_inform";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = null;//连接
        Channel channel = null;//通道

        /**
         * 生产者
         * 声明交换机exchange_fanout_inform
         * 声明两个队列并绑定到指定到交换机,绑定是不需要指定routingkey
         * 发送消息时不需要指定routingkey
         */
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("47.99.79.246");
            factory.setPort(5672);
            factory.setUsername("admin");
            factory.setPassword("admin");
            factory.setVirtualHost("/");
            //rabbitmq默认虚拟机名称为"/",虚拟机相当于一个独立的mq服务
            //创建于rabbitmq服务的TCP连接
            connection = factory.newConnection();
            //创建于exchange(交换机)的通道,每个连接可以创建多个通道,每个通道代表一个会话任务
            channel = connection.createChannel();
            //声明交换机
            channel.exchangeDeclare(EXCHANGE_ROUTING_INFORM, BuiltinExchangeType.DIRECT);


            /**
             * 声明队列,入股rabbitmq中没有此队列将自动创建
             * param1: 队列名称
             * param2: 是否持久化
             * param3: 队列是否独占此链接
             * param4：隊列不在使用是否刪除此队列
             * param5: 队列参数
             */
            channel.queueDeclare(key_queue_inform_email, true,false, false,null);
            channel.queueDeclare(key_queue_inform_sms, true,false, false,null);

            //队列绑定到交换机
            /**
             * 参数明细
             * 1. 队里名称
             * 2.交换机名称
             * 3.路由key
             */
            channel.queueBind(key_queue_inform_email,EXCHANGE_ROUTING_INFORM,key_queue_inform_email);
            channel.queueBind(key_queue_inform_sms,EXCHANGE_ROUTING_INFORM,key_queue_inform_sms);


            /**
             * 详细发布方法
             *param1: Exchange的名称,如果没有指定,则使用默认的交换机
             *param2: routingKey, 消息的路由key,是用于Exchange将消息转发到指定的消息队列
             *param3: 消息抱哈的属性
             *param4: 消息体
             */
            /**
             * 這裏沒有指定交换机,消息将发送给默认的交换机,每个队列也会绑定那个默认的交换机,但是
             * 不能显示绑定或者解除绑定
             * 默认交换机,routingKey等于队列名称
             *
             * 订阅发布模式会将消息分别发送给交换机绑定的消息队列
             *
             * 路由件模式: rountingkey 更具key名称将消息转发到具体的队列,这里填写队列名称表示消息将发送到此队列
             */
            for (int i =10 ; i>=0 ;i--){
                //发送消息
                String message = i+"-你好世界 email inform";
                channel.basicPublish(EXCHANGE_ROUTING_INFORM,key_queue_inform_email,null,message.getBytes());
                System.out.println(i+"-发送消息: "+message+".");
            }

            for(int j = 10 ; j>=0;j--){
                //发送短信消息
                String message = "sms inform to user"+ j;
                //向交换机发送消息
                channel.basicPublish(EXCHANGE_ROUTING_INFORM,key_queue_inform_sms,null,message.getBytes());
                System.out.println("send message is : "+message+" .");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (channel!=null) {
                channel.close();
            }
            if (connection != null){
                connection.close();

            }
        }

    }
}
