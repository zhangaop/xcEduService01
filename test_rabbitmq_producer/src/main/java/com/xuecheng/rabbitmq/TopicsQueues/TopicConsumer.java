package com.xuecheng.rabbitmq.TopicsQueues;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-09-15-09-38
 */
public class TopicConsumer {
    private  static final String QUEUE = "HELLOWORLD";
    private static final String QUEUE_INFORM_EMAIL = "topic_queue_inform_email";
    private static final String QUEUE_INFORM_SMS = "topic_queue_inform_sms";
    //不处理路由键交换机名称
    private static final String EXCHANGE_FANOUT_INFORM = "topic_exchange_fanout_inform";
    private static final String EXCHANGE_TOPTIC_INFORM = "topic_exchange_fanout_inform";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        //设置mq所在服务器的地址的端口
        factory.setPort(5672);
        factory.setHost("www.zlblog.club");
        factory.setUsername("zl");
        factory.setPassword("123456");
        //建立连接
        Connection connection = factory.newConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //声明交换机
        channel.exchangeDeclare(EXCHANGE_TOPTIC_INFORM,BuiltinExchangeType.TOPIC);
        //声明队列
        channel.queueDeclare(QUEUE_INFORM_EMAIL,true,false,false,null);
        channel.queueDeclare(QUEUE_INFORM_SMS,true,false,false,null);

        //绑定队列
        //绑定队列
        channel.queueBind(QUEUE_INFORM_EMAIL,EXCHANGE_TOPTIC_INFORM,"inform.*");
        // channel.queueBind(QUEUE_INFORM_EMAIL,EXCHANGE_FANOUT_INFORM,"inform.*");

        //定义消费方法
        DefaultConsumer consumer = new DefaultConsumer(channel){
            /**
             * 消费者接收消息调用此方法
             * @param consumerTag 消费者标签,在channel,basicConsume()去指定
             * @param envelope 消息包的内容,可从中获取消息id,消息routingKey,交换机,消息和重
             *                 传标志(收到消息失败后是否亚重新发送)
             * @param properties
             * @param body
             * @throws IOException
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                //交换机
                String exchange = envelope.getExchange();
                //路由
                String routingKey = envelope.getRoutingKey();
                //消息id
                long deliveryTag = envelope.getDeliveryTag();
                //消息内容
                String msg = new String(body,"utf-8");
                System.out.println("111接收的消息: "+msg);
            }
        };

        /**
         * 监听队列String Queue ,boolean autoAck, consumer callback
         * 参数明细
         * 1.队列名称
         * 2.是否自动回复,设置ture为表示消息接收到自动向mq回复消息都接收到了,mq接收到回复会删除消息,设置为false这需要手动删除
         * 3.消费消息的方法,消费者接收到消息后调用此方法
         *
         */


            channel.basicConsume(QUEUE_INFORM_SMS,true,consumer);



    //     DefaultConsumer consumer1 = new DefaultConsumer(channel){
    //         /**
    //          * 消费者接收消息调用此方法
    //          * @param consumerTag 消费者标签,在channel,basicConsume()去指定
    //          * @param envelope 消息包的内容,可从中获取消息id,消息routingKey,交换机,消息和重
    //          *                 传标志(收到消息失败后是否亚重新发送)
    //          * @param properties
    //          * @param body
    //          * @throws IOException
    //          */
    //         @Override
    //         public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
    //             //交换机
    //             String exchange = envelope.getExchange();
    //             //路由
    //             String routingKey = envelope.getRoutingKey();
    //             //消息id
    //             long deliveryTag = envelope.getDeliveryTag();
    //             //消息内容
    //             String msg = new String(body,"utf-8");
    //             System.out.println("222接收的消息: "+msg);
    //         }
    //     };
    //
    //     /**
    //      * 监听队列String Queue ,boolean autoAck, consumer callback
    //      * 参数明细
    //      * 1.队列名称
    //      * 2.是否自动回复,设置ture为表示消息接收到自动向mq回复消息都接收到了,mq接收到回复会删除消息,设置为false这需要手动删除
    //      * 3.消费消息的方法,消费者接收到消息后调用此方法
    //      *
    //      */
    //
    //
    //     channel.basicConsume(QUEUE_INFORM_SMS,true,consumer1);
    //
    }



}
