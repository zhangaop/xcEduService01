package com.xuecheng.rabbitmq.TopicsQueues;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-09-14-20-19
 *
 * topic  路由模式 routingkey
 *
 * 星号井号代表通配符
 * 星号代表多个单词,井号代表一个单词
 * 路由功能添加模糊匹配
 * 消息产生者产生消息,把消息交给交换机
 * 交换机根据key的规则模糊匹配到对应的队列,由队列的监听消费者接收消息消费
 */
public class TopicProducer {
    private static final String QUEUE = "HELLO_WORLD";
    private static final String QUEUE_INFORM_EMAIL = "topic_queue_inform_email";
    private static final String QUEUE_INFORM_SMS = "topic_queue_inform_sms";
    //不处理路由键交换机名称
    private static final String EXCHANGE_FANOUT_INFORM = "topic_exchange_fanout_inform";
    private static final String EXCHANGE_TOPTIC_INFORM = "topic_exchange_fanout_inform";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = null;//连接
        Channel channel = null;//通道

        /**
         * 生产者
         * 声明交换机exchange_fanout_inform
         * 声明两个队列并绑定到指定到交换机,绑定是不需要指定routingkey
         * 发送消息时不需要指定routingkey
         */
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("47.99.79.246");
            factory.setPort(5672);
            factory.setUsername("admin");
            factory.setPassword("admin");
            factory.setVirtualHost("/");
            //rabbitmq默认虚拟机名称为"/",虚拟机相当于一个独立的mq服务
            //创建于rabbitmq服务的TCP连接
            connection = factory.newConnection();
            //创建于exchange(交换机)的通道,每个连接可以创建多个通道,每个通道代表一个会话任务
            channel = connection.createChannel();
            //声明交换机
            channel.exchangeDeclare(EXCHANGE_TOPTIC_INFORM, BuiltinExchangeType.TOPIC);


            /**
             * 声明队列,入股rabbitmq中没有此队列将自动创建
             * param1: 队列名称
             * param2: 是否持久化
             * param3: 队列是否独占此链接
             * param4：隊列不在使用是否刪除此队列
             * param5: 队列参数
             */
            channel.queueDeclare(QUEUE_INFORM_EMAIL, true,false, false,null);
            channel.queueDeclare(QUEUE_INFORM_SMS, true,false, false,null);

            //绑定队列
            // channel.queueBind(QUEUE_INFORM_SMS,EXCHANGE_FANOUT_INFORM,"info.#.topic");
            // channel.queueBind(QUEUE_INFORM_EMAIL,EXCHANGE_FANOUT_INFORM,"info.*");

            /**
             * 详细发布方法
             *param1: Exchange的名称,如果没有指定,则使用默认的交换机
             *param2: routingKey, 消息的路由key,是用于Exchange将消息转发到指定的消息队列
             *param3: 消息抱哈的属性
             *param4: 消息体
             */
            /**
             * 這裏沒有指定交换机,消息将发送给默认的交换机,每个队列也会绑定那个默认的交换机,但是
             * 不能显示绑定或者解除绑定
             * 默认交换机,routingKey等于队列名称
             *
             * 订阅发布模式会将消息分别发送给交换机绑定的消息队列
             */
            for (int i =10 ; i>=0 ;i--){
                //发送消息
                String message = i+"-sms你好世界"+System.currentTimeMillis();
                channel.basicPublish(EXCHANGE_TOPTIC_INFORM,"inform.sms",null,message.getBytes());
                System.out.println(i+"-发送消息: "+message+".");
            }

            for (int j =10 ; j>=0 ;j--){
                //发送消息
                String message = j+"-email你好世界"+System.currentTimeMillis();
                channel.basicPublish(EXCHANGE_TOPTIC_INFORM,"inform.email",null,message.getBytes());
                System.out.println(j+"-发送消息: "+message+".");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (channel!=null) {
                channel.close();
            }
            if (connection != null){
                connection.close();

            }
        }

    }
}
