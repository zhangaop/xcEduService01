package com.xuecheng.rabbitmq.simpleWoekQueues;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-09-14-20-19
 */
public class Peoducer {
    private static final String QUEUE = "HELLOWORLD";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = null;//连接
        Channel channel = null;//通道
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("47.99.79.246");
            factory.setPort(5672);
            factory.setUsername("admin");
            factory.setPassword("admin");
            factory.setVirtualHost("/");
            //rabbitmq默认虚拟机名称为"/",虚拟机相当于一个独立的mq服务
            //创建于rabbitmq服务的TCP连接
            connection = factory.newConnection();
            //创建于exchange(交换机)的通道,每个连接可以创建多个通道,每个通道代表一个会话任务
            channel = connection.createChannel();
            /**
             * 声明队列,入股rabbitmq中没有此队列将自动创建
             * param1: 队列名称
             * param2: 是否持久化
             * param3: 队列是否独占此链接
             * param4：隊列不在使用是否刪除此队列
             * param5: 队列参数
             */
            channel.queueDeclare(QUEUE, true,false, false,null);
            String message = "你好世界"+System.currentTimeMillis();
            /**
             * 详细发布方法
             *param1: Exchange的名称,如果没有指定,则使用默认的交换机
             *param2: routingKey, 消息的路由key,是用于Exchange将消息转发到指定的消息队列
             *param3: 消息抱哈的属性
             *param4: 消息体
             */
            /**
             * 這裏沒有指定交换机,消息将发送给默认的交换机,每个队列也会绑定那个默认的交换机,但是
             * 不能显示绑定或者解除绑定
             * 默认交换机,routingKey等于队列名称
             */
            for (int i =1000000 ; i>=0 ;i--){

                channel.basicPublish("",QUEUE,null,message.getBytes());
                System.out.println(i+"-发送消息: "+message+".");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (channel!=null) {
                channel.close();
            }
            if (connection != null){
                connection.close();

            }
        }

    }
}
