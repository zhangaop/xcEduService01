package com.xuecheng.manage_cms.controller;

import com.xuecheng.api.cms.CmsPageControllerApi;
import com.xuecheng.framework.domain.cms.CmsPage;
import com.xuecheng.framework.domain.cms.request.QueryPageRequest;
import com.xuecheng.framework.domain.cms.response.CmsPageResult;
import com.xuecheng.framework.model.response.CommonCode;
import com.xuecheng.framework.model.response.QueryResponseResult;
import com.xuecheng.framework.model.response.QueryResult;
import com.xuecheng.framework.model.response.ResponseResult;
import com.xuecheng.manage_cms.service.PageService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Administrator
 * @version 1.0
 * @create 2018-09-12 17:24
 **/
@RestController
@RequestMapping("/cms/page")
@Slf4j
public class CmsPageController implements CmsPageControllerApi {

    @Autowired
    PageService pageService;

    @Override
    @GetMapping("/list/{page}/{size}")
    public QueryResponseResult findList(@PathVariable("page") int page, @PathVariable("size") int size, QueryPageRequest queryPageRequest) {

/*        //暂时用静态数据
        //定义queryResult
        QueryResult<CmsPage> queryResult =new QueryResult<>();
        List<CmsPage> list = new ArrayList<>();
        CmsPage cmsPage = new CmsPage();
        cmsPage.setPageName("测试页面");
        list.add(cmsPage);
        queryResult.setList(list);
        queryResult.setTotal(1);

        QueryResponseResult queryResponseResult = new QueryResponseResult(CommonCode.SUCCESS,queryResult);
        return queryResponseResult;*/
        //调用service
        return pageService.findList(page, size, queryPageRequest);
    }

    /**
     * 添加页面
     *
     * @param cmsPage 页面参数
     * @return 成功编码
     */
    @Override
    @PostMapping("/add")
    public CmsPageResult add(@RequestBody CmsPage cmsPage) {
        return pageService.add(cmsPage);
    }

    /**
     * 通过页面id获取页面详情
     *
     * @param id 页面id
     * @return cmsPage数据
     */
    @Override
    @GetMapping("/get/{id}")
    public CmsPage findById(@PathVariable("id") String id) {
        return pageService.getById(id);
    }

    /**
     * 通过id，page数据编辑页面数据
     *
     * @param id      页面id
     * @param cmsPage 页面详情
     * @return 结果编码
     */
    @Override
    @PutMapping("/edit/{id}")
    public CmsPageResult edit(@PathVariable("id") String id, @RequestBody CmsPage cmsPage) {
        return pageService.edit(id, cmsPage);
    }

    @Override
    @DeleteMapping("/del/{id}")
    public ResponseResult delete(@PathVariable("id") String id) {
        return pageService.del(id);
    }

    @GetMapping("/getTime")
    public TimeDate print() {
        TimeDate timeDate = new TimeDate(new Date());
        return timeDate;
    }

    /**
     * 导出模版
     *
     * @param response
     */
    @GetMapping("/exportExcel/model")
    public boolean export(HttpServletResponse response) {
        try {
            //模拟从数据库获取需要导出的数据
            List<User> personList = new ArrayList<>();
            for (int i = 10; i >= 0; i--) {
                User user = new User();
                user.setName("张磊" + i);
                user.setEmail("fhsdhg@-" + i);
                user.setIdcardAdress("zxhdn主驾驶本=" + i);
                personList.add(user);
            }

            FileWithExcelUtil.exportExcel(personList, "客户信息表", "客户表", User.class, "客户表.xls", response);
            return true;
        } catch (Exception e) {
            log.info("getCustomerPage", e);
            return false;
            // TODO: handle exception
        }
    }

    @GetMapping("/exportExcel/model1")
    @ApiOperation(value = "不设置表头")
    public boolean export1(HttpServletResponse response) {
        try {
            //模拟从数据库获取需要导出的数据
            List<User> personList = new ArrayList<>();
            for (int i = 10; i >= 0; i--) {
                User user = new User();
                user.setName("张一磊" + i);
                user.setEmail("fhsdhg@-" + i);
                user.setIdcardAdress("zxhdn主驾驶本=" + i);
                personList.add(user);
            }

            FileWithExcelUtil.exportExcel(personList, "客户信息表", "客户表", User.class, "客户表.xls", false, response);
            return true;
        } catch (Exception e) {
            log.info("getCustomerPage", e);
            return false;
            // TODO: handle exception
        }

    }
}
