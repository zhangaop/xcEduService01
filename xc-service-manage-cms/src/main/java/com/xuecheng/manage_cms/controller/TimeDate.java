package com.xuecheng.manage_cms.controller;

import com.xuecheng.api.annotation.DateFormateTemplate;

import java.util.Date;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-14-16-55
 */
public class TimeDate {
    private Date date;
    private String str;

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    @DateFormateTemplate
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public TimeDate(Date date) {
        this.date = date;
    }
}

