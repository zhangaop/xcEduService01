package com.zl.testMongoDB;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-20-17-17
 */
@Data
public class listNode {
    int var;
    listNode next;

    public listNode(int var) {
        this.var = var;
    }

    public static void main(String[] args) throws IOException {

        InputStreamReader stream = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(stream);
        String s = reader.readLine();
        //输入参数
        double a= Double.valueOf(s);
        int b = (int) a;
        System.out.println("b = " + b);
        System.out.println("a = " + a);

        if((a-b)>=0.5){
            b = (int) (a+0.5);
        }
        System.out.println("取整 b = "+b);



    }
}
