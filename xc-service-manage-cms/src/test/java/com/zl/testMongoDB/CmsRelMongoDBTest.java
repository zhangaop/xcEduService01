package com.zl.testMongoDB;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.xuecheng.manage_cms.ManageCmsApplication;
import com.xuecheng.manage_cms.controller.CmsPageController;
import org.bson.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;
@RunWith(SpringRunner.class)
@SpringBootTest(classes = ManageCmsApplication.class)
@WebAppConfiguration
public class CmsRelMongoDBTest {
    private MockMvc Mocc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Before
    public void setup(){
        Mocc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    @Test
    public void connectMongoDBTest(){
    //创建mongodb的客户端
        MongoClient client = new MongoClient("localhost",27017);
    //或者采用连接字符串
    //     MongoClientURI clientURI = new MongoClientURI("mongodb://root:root@loaclhost:27017");
        // MongoClient client1 = new MongoClient(clientURI);
        //连接数据库
        MongoDatabase database =client.getDatabase("test");
        //连接collection
        MongoCollection<Document> collection = database.getCollection("hello");
        //查询第一个文档
        Document first = collection.find().first();
        //获取文件内容,得到json串


        System.out.println();

        System.out.println(first.toJson());

        System.out.println();
    }

    /**
     * 1.
     */
    @Test
    public void time() throws Exception {
        TestTime testTime = new TestTime(new Date());
        System.out.println(testTime.getCreate());
        Mocc.perform(MockMvcRequestBuilders.get("/cms/page/getTime")
        .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());

    }
}
