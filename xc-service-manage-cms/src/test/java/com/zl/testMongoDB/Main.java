package com.zl.testMongoDB;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        if((input = reader.readLine())!=null){
            float a = Float.valueOf(input);
            System.out.println(Math.round(a));
        }
    }
}
