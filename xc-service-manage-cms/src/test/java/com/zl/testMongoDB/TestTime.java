package com.zl.testMongoDB;

import com.xuecheng.api.annotation.DateFormateTemplate;
import lombok.Data;

import java.util.Date;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-14-15-08
 */

public class TestTime {

    @DateFormateTemplate()
    private Date create;

    public TestTime(Date create) {
        this.create = create;
    }
    @DateFormateTemplate()
    public Date getCreate() {
        return create;
    }

    public void setCreate(Date create) {
        this.create = create;
    }
}
