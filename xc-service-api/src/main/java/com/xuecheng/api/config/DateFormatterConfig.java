package com.xuecheng.api.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xuecheng.api.annotation.MyAnnotationIntrospector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.TimeZone;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-12-15-45
 */



@Configuration
public class DateFormatterConfig {


    @Bean
    public MyAnnotationIntrospector myAnnotationIntrospector(){
        return new MyAnnotationIntrospector();
    }

   @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter(){
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();
        ObjectMapper objectMapper = messageConverter.getObjectMapper();
        objectMapper.setTimeZone(TimeZone.getDefault());
        objectMapper.setAnnotationIntrospector(myAnnotationIntrospector());
        return messageConverter;
    }
}
