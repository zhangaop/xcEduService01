package com.xuecheng.api.annotation;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.time.DateUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-06-16-48
 */
@Component
@Aspect
@Slf4j
public class DateFormateImpl {

    @Around("@annotation(DateFormateTemplate)")
    public Object around(ProceedingJoinPoint joinPoint){
        log.info("进入日期格式化的注解实现类");
        Object[] args = joinPoint.getArgs();
        log.info("获取切面的参数 date={}",args);
        DateFormateTemplate annotation = args.getClass().getAnnotation(DateFormateTemplate.class);
        String pattern = annotation.pattern();
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        String date = format.format(args[0]);


//        DateFormateTemplate annotation = args.getClass().getAnnotation(DateFormateTemplate.class);
        return null;
    }


    public static void main(String[] args) throws ParseException {
        System.out.println(DateUtils.parseDate(DateFormat.getDateTimeInstance().format(new Date()),new String[]{"yyyy-MM-dd HH:mm:ss"}));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = formatter.format(new Date());
        System.out.println(format);
        Date parse = formatter.parse(format);
        System.out.println(parse);


    }

}
