package com.xuecheng.api.annotation;

import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-12-15-11
 */
public class MyAnnotationIntrospector extends JacksonAnnotationIntrospector {

    @Override
    public Object findSerializer(Annotated a) {
        //经测试只对方法有用,即getter上的注解
        if(a instanceof AnnotatedMethod){
            DateFormateTemplate annotation = a.getAnnotated().getAnnotation(DateFormateTemplate.class);
            if(annotation != null){
                return new DateTimeSerializer(annotation.pattern());
            }
        }
        return super.findSerializer(a);
    }
}
