package com.xuecheng.api.annotation;

import java.lang.annotation.*;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-06-16-06
 * DateFormateTemplatesss
 * 该注解目的是为了演示@Repeatable注解的作用
 * 通过当前注解创建一个目标注解的集合,使其可以在一个type上可以多次被使用
 */
@Target({ElementType.TYPE,ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateFormateTemplatesss {

    DateFormateTemplate[] value() default {};
}
