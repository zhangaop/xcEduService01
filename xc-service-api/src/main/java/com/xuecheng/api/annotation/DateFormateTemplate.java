package com.xuecheng.api.annotation;

import java.lang.annotation.*;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-06-15-45
 */

/**
 * 注解的作用类型
 * PARAMETER 参数声明
 */
@Target({ElementType.FIELD,ElementType.METHOD})
/**
 * 注解的保留时间
 * RUNTIMME  VM 将在运行期也保留注解，因此可以通过反射机制读取注解的信息。
 */
@Retention(RetentionPolicy.RUNTIME)
/**
 * 允许子类继承父类的注解
 * 允许注解被使用一次或多次
 */
//@Repeatable(DateFormateTemplatesss.class)
//@Inherited
@Documented
public @interface DateFormateTemplate {
    /**
     * 日期格式化
     * yyyy-MM-dd HH:mm:ss
     * yyyy年MM月dd日 HH时mm分ss秒
     * 等等
     */
    String pattern() default "yyyy-MM-dd";

}
