package com.xuecheng.api.annotation;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @Author: zhanglei2
 * @CreatTime: 2020-08-07-14-37
 * Json串行化处理器
 */
public class DateTimeSerializer extends JsonSerializer<Date> {
    //用于存储日期串行化格式
    private final String key;

    public DateTimeSerializer(String key) {
        super();
        this.key = key;
    }

    /**
     *
     * @param date 日期
     * @param jsonGenerator 基类，定义了用于编写JSON内容的公共API。实例正在使用的工厂方法创建]sonFactory实例。
     * @param serializerProvider 序列化提供着
     * @throws IOException
     */
    @Override
    public void serialize(Date date,
                          JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException {
        String output = StringUtils.EMPTY;
        if(date!=null){
            //将日期转换为字符串 GMT+8
            output = LocalDateTime.ofInstant(date.toInstant(),
                    ZoneId.systemDefault()).format(DateTimeFormatter.ofPattern(key));
        //new SimpleDateFormat(key).format(date);
        }
        //输出转换结果
        jsonGenerator.writeString(output);
    }
}
